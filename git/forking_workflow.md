Example Commands for Our Forking Workflow
=========================================


## Official Repository

Owner/Maintainer of Official Repository -- clone it:

	git clone https://owner@bitbucket.org/owner/project.git
	
Create our 2-fork minimum workflow strategy:

	git branch develop
	
This gives us 2 main branches:

*  master   --   this is the main deployment/production branch.  Code on this branch will be automatically deployed to the web
*  develop (or dev or development)   --   this is the development branch.  It is a holding zone for code awaiting to be merged into master.  At the end of a Sprint, the maintainer will merge this into master.  Later once we're doing testing, we can have a separate testing branch between develop and master.  **All code written at any time should be done in a feature branch off of develop.**

When originally setting things up it is easiest if the official maintainer creates a readme and the folder structure to be used.  Create folders and place a readme in each folder that describes what it is for.  Add these using a feature branch off develop, add/commit and merge into develop.  Then push develop.  When ready, merge into master and push master.  All changes on develop need to be pushed to the official repo so team members can have access to the latest version of develop.

	# set up develop
	git checkout develop
	touch README.md
	git add README.md
	git commit -m "creating a home page readme and advancing develop branch to make it clear"
	
	# create feature branch, work on it then merge into develop
	git branch feature_setup
	git checkout feature_setup
	mkdir -p Milestones/Milestone{1..5}
	touch Milestones/Milestone{1..5}/README.md
	git add .
	git commit -m "initial setup of project repository"
	git checkout develop
	git merge feature_setup
	git branch -d feature_setup    # no need for this branch locally or on remote, so delete it
	git push origin develop        # share code with team
	
## Forked Repository

Each team member/contributor forks the official repository and then clones it:

	# fork it (and add your name as a suffix to the repo name if you want to make it really clear that this is a fork)
	git clone https://contributor@bitbucket.org/contributor/project-contrib.git
	
Add a remote called *upstream* to the official repo.  Do the same, when needed, to your team-mates forks:

	git remote add upstream https://owner@bitbucket.org/owner/project.git

If you need to fetch any new branches from upstream, do this before trying to check them out

	git fetch upstream
	
Now, for the normal workflow:

	# work in a feature branch off develop
	git checkout develop
	git branch ft_104_scot     # use a naming scheme, here it's ft[eature]_[user story or work item ID]_[developer name] or make up your own
	git checkout ft_104_scot
	
	# work on your feature, test it, finish it
	# git add .
	# git commit -m "add individual user accounts and roles, includes Admin role"
	
	# Very important! Make sure this won't cause a merge conflict.  Update develop, merge it into our feature branch to see or fix conflicts, then proceed
	git checkout develop
	git pull upstream develop
	git checkout ft_104_scot
	git merge develop
	# fix merge conflicts, add/commit them on ft_104_scot
	# ready for a conflict free pull request, so push our feature branch to our remote
	
	git push origin ft_104_scot
	
	# issue pull request on Bitbucket.  Select branch ft_104_scot as source and develop as destination
	# pull request accepted and merged into develop on upstream, so we need to get it and update our own remote
	git checkout develop
	git pull upstream develop
	git push origin develop
	# we should now be in perfect sync
	
	
What if the pull request is rejected?  In our case that would happen if the team member didn't merge the latest develop into it first in an attempt to fix them themselves.  The cool thing about pull requests it that it isn't a one-time-thing.  If changes are needed, you can add commit and push changes to the feature branch while the pull request is pending.  Once it's ready it can be accepted.

## Official Repository

Processing a pull request that has no merge conflicts is easy -- you can click the merge button.  If it has a conflict then you'll need to do one of two things:

1. Tell the contributor to fix the problem and update their feature branch  (best option!)
2. Fix it yourself

Here's how to do the second one.  The official adds the contributor's repo as a remote so she can fetch the feature branch from it (you'd name these with your team-mates names, not contrib):

	git remote add contrib https://contributor@bitbucket.org/contributor/project-contrib.git
	# fetch branch that is used in the pull request
	git fetch contrib
	# branch will come in with the name: remote/branchname, so we can check it out and use it like this:
	git checkout contrib/ft_104_scot
	# initiate merge and fix problems
	git merge develop
	#fix
	git add .
	git commit -m "fixed merge conflicts due to ..."
	# perform the official merge into develop, should be a fast forward merge
	git checkout develop
	git merge contrib/ft_104_scot
	# share with everyone
	git push origin develop
	
Pushing the merged work on develop actually finishes the pull request.  Go back to Bitbucket and confirm.  That's it.  Now everyone else can pull from upstream develop to get the latest code. :-)